from django.contrib import admin

from foo.models import Apple, RedFruit


@admin.register(Apple)
class Apple(admin.ModelAdmin):
    list_display = ('color', 'taste')


@admin.register(RedFruit)
class RedFruit(admin.ModelAdmin):
    list_display = ('price',)
