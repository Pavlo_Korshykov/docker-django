from django.http import HttpResponse
from django.conf import settings
import redis


redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                   port=settings.REDIS_PORT,
                                   db=0)


def index(request):
    request_count = redis_instance.get('request_count')
    if request_count:
        request_count = int(request_count)
        new_value = request_count + 1
        redis_instance.set('request_count', new_value)
    else:
        request_count = 1
        redis_instance.set('request_count', 2)
    return HttpResponse(f'Request count: {request_count}')


def hi(request):
    return HttpResponse('Easy')
