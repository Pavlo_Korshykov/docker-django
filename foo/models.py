from django.db import models


class Fruit(models.Model):
    color = models.CharField(max_length=100)

    class Meta:
        abstract = True


class Apple(Fruit):
    taste = models.CharField(max_length=100)


class RedFruit(Fruit):
    price = models.IntegerField()

